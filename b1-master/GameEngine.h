#ifndef GAMEENGINE_HPP
#define GAMEENGINE_HPP

#include <stack>
#include <vector>
#include <SFML/Graphics.hpp>
class GameState;

class GameEngine
{
public:
	std::stack<GameState*> states;

	sf::RenderWindow window;

	void pushState(GameState* state);

	void popState();
	void changeState(GameState* state);
	GameState* peekState();
	
	void gameLoop();

	GameEngine();
	~GameEngine();

private:
	
};

#endif // !GAMEENGINE_HPP
