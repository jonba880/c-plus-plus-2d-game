#ifndef GAMESTATESTART_H
#define GAMESTATESTART_H

#include <SFML/Graphics.hpp>
#include "Graphic.h"
#include "GameState.h"

class GameStateStart : public GameState
{
public:

	void draw(const float dt);
	void update(const float dt);
	void handleInput();

	GameStateStart(GameEngine* game);

private:
	void loadgame();
	void showControls();
};

#endif // !GAMESTATESTART_H
