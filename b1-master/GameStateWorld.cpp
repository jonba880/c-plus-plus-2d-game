#include "GameStateWorld.h"
#include "GameStatePause.h"
#include "GameStateWin.h"
#include "GameState.h"
#include "World.h"
#include "Information.h"
#include <vector>
#include <unordered_set>
#include <SFML/Graphics.hpp>
#include <iostream>

void GameStateWorld::draw(const float dt)
{
	game->window.clear(sf::Color::Black);
	game->window.draw(_background);


	for (Player &v : players) 
	{
		for (ProjPointer proj : v.getProjectiles())
		{
			if (proj->isAlive())
			{
				this->game->window.draw(proj->getSprite());
			}
		}
		if(v.getCharacter().isAlive())
			this->game->window.draw(v.getCharacterSprite());
	}

	for (Box b : world.getBoxes())
	{
		this->game->window.draw(b.getSprite());
	}

	info.drawInfo(&this->game->window);

	return;
}


Player &GameStateWorld::getOpponent(int i) {
	return players.at((i+1) % 2);
}

void GameStateWorld::updateScore(int player_index, float dt) {
	std::unordered_set<int> enemy_body;
	int diameter = round(players.at(player_index).getGraphic().getTexture()->getSize().x *  2/3);
	getCircleArea(getOpponent(player_index).getGraphic() ,enemy_body, diameter);


	for (ProjPointer proj : players.at(player_index).getProjectiles()) {
		int key = hash(proj->getPosition().x, proj->getPosition().y);
		if (proj->isAlive() && enemy_body.find(key) != enemy_body.end() && getOpponent(player_index).getCharacter().isAlive()) {
			players.at(player_index).increaseScore();
			getOpponent(player_index).playerRespawn(world.getRandomSpawn());
			getOpponent(player_index).die();
			proj->die();
			break;
		}
	}

	if (!players.at(player_index).getCharacter().isAlive())
		players.at(player_index).getRespawnTimer() += dt;

	if (players.at(player_index).getRespawnTimer() >= RESPAWN_DELAY)
	{
		players.at(player_index).getRespawnTimer() = 0;
		players.at(player_index).live();
	}

	

}

void GameStateWorld::update(const float dt)
{
	if(jump)
	{ 
		this->game->popState();
		return;
	}

	updateScore(0, dt);
	updateScore(1, dt);

	for (Player &v : players) 
	{
		v.updateProjectile(world);
		v.actionListener(world);
		v.freeze();
	}
	if (winnerExist())
	{
		wingame();
		players.at(0).resetScore();
		players.at(1).resetScore();
		jump = true;
	}
	return;
}

void GameStateWorld::handleInput()
{
	sf::Event event;
	while (this->game->window.pollEvent(event))
	{
		switch (event.type)
		{
			/* Close the window */
		case sf::Event::Closed:
		{
			game->window.close();
			break;
		}

		/* Input */
		case sf::Event::KeyPressed:
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
				this->pausegame();
			break;
		}
		default:
			break;
		}
	}
}

GameStateWorld::GameStateWorld(GameEngine * game)
{
	world.setScreenSize(game->window.getSize());
	this->game = game;
	info.set(players.at(0).getScoreRef(), players.at(1).getScoreRef(), players.at(0).getAmmoRef(), players.at(1).getAmmoRef());
	setBackground("textures/worldmap.png");
	players.at(0).playerRespawn(world.getRandomSpawn());
	players.at(1).playerRespawn(world.getRandomSpawn());
	while(players.at(0).getGraphic().getPosition() == players.at(1).getGraphic().getPosition())
		players.at(1).playerRespawn(world.getRandomSpawn());

}

void GameStateWorld::pausegame()
{
	this->game->pushState(new GameStatePause(this->game));
}

void GameStateWorld::wingame()
{
	this->game->pushState(new GameStateWin(this->game, players.at(0).getScore(), players.at(1).getScore()));
}

bool GameStateWorld::winnerExist()
{
	return (players.at(0).getScore() == 3 || players.at(1).getScore() == 3);
}

