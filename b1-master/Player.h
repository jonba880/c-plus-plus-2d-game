#ifndef PLAYER_HPP
#define PLAYER_HPP
#include "DynamicObject.h"
#include "World.h"

typedef std::shared_ptr<Projectile> ProjPointer;
typedef sf::Keyboard::Key Key;


struct Controls {
	Key left, right, forward, backward, shoot;
	Controls(Key left, Key right, Key forward, Key backward, Key shoot)
		: left{ left }, right{ right }, forward{ forward }, backward{ backward }, shoot{ shoot } {}
};


class Player {
private:
	Character c;
	Controls player_controls;
	int score;
	int ammo;
	bool initReload;
	unsigned ammo_key;
	std::vector<ProjPointer> projectiles;
	float respawnTimer{ 0 };

	int freeze_time{ 0 }, freeze_time2{ 0 }, freeze_time3{ 0 }, freeze_time4{ 0 };
public:

	Player(std::string character_img, std::string projectile_img, Controls controls) : player_controls{ controls }, c{ character_img }, ammo_key{ N_OF_PROJECTILES }, score{ 0 }, ammo{ 10 }, initReload{ false } {


		initializeProjectiles(projectile_img);
	}
	Graphic& getGraphic() {
		return c.getGraphic();
	}

	void playerRespawn(sf::Vector2f positionSpawn) {

		c.setPosition(positionSpawn);
		resetAmmo();
		c.setDirection(rand() % 15);
		c.setRotation();
	}




	float& getRespawnTimer() { return respawnTimer; }
	void die() { c.die(); }
	void live() { c.live(); }

	void freeze() {
		freeze_time4 = (freeze_time4 <= 0) ? 0 : freeze_time4 - 1;
		freeze_time3 = (freeze_time3 <= 0) ? 0 : freeze_time3 - 1;
		freeze_time2 = (freeze_time2 <= 0) ? 0 : freeze_time2 - 1;
		freeze_time  = (freeze_time  <= 0) ? 0 : freeze_time  - 1;

	}

	void initializeProjectiles(std::string projectile_img) {
		sf::Texture * texture = new sf::Texture;
		texture->loadFromFile(projectile_img);
		for (int i = 0; i < N_OF_PROJECTILES; i++)
			projectiles.push_back(ProjPointer(new Projectile(texture)));
	}

	void updateProjectile(World &world) {
		for (ProjPointer p : projectiles) {
			if (world.onForbiddenGround(p->getPosition())) { 
				p->die();
			} else if (p->isAlive()) {
				p->moveForward();
			}
		}
	}

	void actionListener(World &world) {
		if (c.isAlive()) {
			if (freeze_time == 0) {
				if (sf::Keyboard::isKeyPressed(player_controls.left)) {
					c.rotateLeft();
					freeze_time = ROTATION_DELAY;
				} if (sf::Keyboard::isKeyPressed(player_controls.right)) {
					c.rotateRight();
					freeze_time = ROTATION_DELAY;
				}
			}

			if (freeze_time2 == 0) {
				if (sf::Keyboard::isKeyPressed(player_controls.forward)) {
					if (!world.onForbiddenGround(c.getForwardPos())) {
						c.moveForward();
					}
					freeze_time2 = MOVING_DELAY;
				} if (sf::Keyboard::isKeyPressed(player_controls.backward)) {
					if (!world.onForbiddenGround(c.getBackwardPos())) {
						c.moveBackwards();
					}
					freeze_time2 = MOVING_DELAY;
				}
			}

			if (freeze_time4 == 0) {
				if (freeze_time3 == 0 && ammo > 0) {
					if (sf::Keyboard::isKeyPressed(player_controls.shoot)) {
						ammo_key = (ammo_key + 1) % N_OF_PROJECTILES;
						ProjPointer proj = projectiles.at(ammo_key);
						proj->setDirection(c.getDirection());
						proj->setPosition(c.getPosition().x - 5, c.getPosition().y - 5);
						proj->live();
						proj->moveForward(5, 5);
						freeze_time3 = SHOOTING_DELAY;
						decreaseAmmo();
						if (ammo == 0)
						{
							reload();
						}
					}
				}
				if (initReload && freeze_time4 == 0)
				{
					resetAmmo();
					initReload = false;
				}
			}
		}
	}
	void reload()
	{
		freeze_time4 = RELOAD_DELAY;
		initReload = true;
	}

	//Score help functions
	const int getScore() {
		return score;
	}
	int* getScoreRef() {
		return &score;
	}
	void increaseScore() {
		if (score < 5) {
			++score;
		}
	}
	void decreaseScore() {
		if (score > 0) {
			--score;
		}
	}
	void resetScore() {
		score = 0;
	}

	//Ammo help functions
	const int getAmmo() {
		return ammo;
	}
	int* getAmmoRef() {
		return &ammo;
	}
	void decreaseAmmo() {
		if (ammo > 0) {
			--ammo;
		}
	}
	void resetAmmo() {
		ammo = 10;
	}
	const Graphic& getCharacterSprite() {
		return c.getSprite();
	}

	const ProjPointer getProjectile() {
		return projectiles.at(ammo_key);
	}

	const std::vector<ProjPointer>& getProjectiles() {
		return projectiles;
	}

	Character getCharacter()
	{
		return c;
	}

	const Graphic& getProjectileSprite() {
		return 	projectiles.at(ammo_key)->getSprite();
	}



};

#endif
