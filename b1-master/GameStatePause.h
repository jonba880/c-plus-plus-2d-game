#ifndef GAMESTATEPAUSE_H
#define GAMESTATEPAUSE_H

#include <SFML/Graphics.hpp>
#include "Graphic.h"
#include "GameState.h"

class GameStatePause : public GameState
{
public:
	void draw(const float dt);
	void update(const float dt);
	void handleInput();

	GameStatePause(GameEngine* game);
	~GameStatePause();
private:
	void jumpback();
	
};

#endif // !GAMESTATEPAUSE_H
