#include "GameEngine.h"
#include "GameState.h"

#include <stack>
#include <iostream>

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>



void GameEngine::pushState(GameState * state)
{
	this->states.push(state);

	return;
}

void GameEngine::popState()
{
	delete this->states.top();
	this->states.pop();

	return;
}

void GameEngine::changeState(GameState * state)
{
	if (!this->states.empty())
		popState();
	pushState(state);
	
	return;
}

GameState * GameEngine::peekState()
{
	if (this->states.empty())
		return nullptr;
	return this->states.top();
}

void GameEngine::gameLoop()
{
	sf::Clock clock;
	while (window.isOpen())
	{
		
		sf::Time elapsed = clock.restart();
		float dt = elapsed.asSeconds();
		if (peekState() == nullptr) continue;
		peekState()->handleInput();
		peekState()->update(dt);
		window.clear(sf::Color::Black);
		peekState()->draw(dt);
		window.display();
	}

}

GameEngine::GameEngine()
{
	
	window.create(sf::VideoMode(1280, 720), "PangPang", sf::Style::Titlebar | sf::Style::Close);
	window.setFramerateLimit(60);
}


GameEngine::~GameEngine()
{
	while (!states.empty())
		popState();
}
