#ifndef GAMESTATEWIN_H
#define GAMESTATEWIN_H

#include <SFML/Graphics.hpp>
#include "Graphic.h"
#include "GameState.h"

class GameStateWin : public GameState
{
public:
	void draw(const float dt);
	void update(const float dt);
	void handleInput();

	GameStateWin(GameEngine* game, int p1, int p2);
	~GameStateWin();
private:
	void jumpback();
};

#endif
