#include <SFML/Graphics.hpp>

#include "GameState.h"
#include "GameStateStart.h"
#include "GameStateWorld.h"
#include "GameStateControls.h"


void GameStateStart::draw(const float dt)
{
	this->game->window.clear(sf::Color::Black);
	this->game->window.draw(_background);
}

void GameStateStart::update(const float dt)
{
	return;
}

void GameStateStart::handleInput()
{
	sf::Event event;

	while (this->game->window.pollEvent(event))
	{
		switch (event.type)
		{
			/* Close the window */
		case sf::Event::Closed:
		{
			game->window.close();
			break;
		}

		/* Input */
		case sf::Event::KeyPressed:
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
				game->window.close();
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
				this->loadgame();
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::C))
			{
				showControls();
				return;
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::E))
				game->window.close();
			break;
		}
		default:
			break;
		}
	}
}

GameStateStart::GameStateStart(GameEngine * game)
{
	this->game = game;
	setBackground("textures/startmenu.png");
}

void GameStateStart::loadgame()
{
	this->game->pushState(new GameStateWorld(this->game));

}

void GameStateStart::showControls()
{
	this->game->pushState(new GameStateControls(this->game));
}