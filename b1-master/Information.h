#ifndef INFORMATION_H
#define INFORMATION_H



#include <SFML/Graphics.hpp>
#include <string>
#include "Constants.h"

class Information
{
public:

	Information()
	{
		font.loadFromFile("textures/times.ttf");
		p1score.setFont(font);
		p1score.setFillColor(sf::Color::Blue);
		p1score.setCharacterSize(60);

		p2score.setFont(font);
		p2score.setFillColor(sf::Color::Red);
		p2score.setCharacterSize(60);

		separator.setFont(font);
		separator.setFillColor(sf::Color::Black);
		separator.setCharacterSize(60);
		separator.setString(":");

		p1ammo.setFont(font);
		p1ammo.setFillColor(sf::Color::Blue);
		p1ammo.setCharacterSize(40);

		p2ammo.setFont(font);
		p2ammo.setFillColor(sf::Color::Red);
		p2ammo.setCharacterSize(40);

	}


	void drawInfo(sf::RenderWindow * window)
	{
		update();

		p1score.setPosition(sf::Vector2f(window->getSize().x / 2 - p1score.getGlobalBounds().width - separator.getGlobalBounds().width, 0));
	//	p1score.setPosition(sf::Vector2f((window->getSize().x / 2) - (p1score.getGlobalBounds().width) - (separator.getGlobalBounds().width / 2) - 20, 0));
		separator.setPosition(sf::Vector2f(((window->getSize().x)/2.0f), -3));
		p2score.setPosition(sf::Vector2f(window->getSize().x / 2 + separator.getGlobalBounds().width * 2 ,0));
		p1ammo.setPosition(sf::Vector2f(0, window->getSize().y - p1ammo.getGlobalBounds().height - 15));
		p2ammo.setPosition(sf::Vector2f(window->getSize().x - p2ammo.getGlobalBounds().width - 15, window->getSize().y - p2ammo.getGlobalBounds().height - 15));
		window->draw(separator);
		window->draw(p1score);
		window->draw(p2score);
		window->draw(p1ammo);
		window->draw(p2ammo);
	}

	void set(int * p1s, int * p2s, int * p1a, int * p2a)
	{
		player1score = p1s;
		player2score = p2s;
		player1ammo = p1a;
		player2ammo = p2a;
	}

	void update()
	{
		p1score.setString(std::to_string(*player1score));
		p2score.setString(std::to_string(*player2score));
		p1ammo.setString(std::to_string(*player1ammo));
		p2ammo.setString(std::to_string(*player2ammo));
	}


private:
	int *player1score, *player2score, *player1ammo, *player2ammo;
	sf::Text p1score, p2score, p1ammo, p2ammo, separator;
	sf::Font font;
};
#endif // !INFORMATION_H			
