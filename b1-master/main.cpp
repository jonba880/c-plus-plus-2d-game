#include "GameEngine.h"
#include "GameStateStart.h"
#include <ctime>
int main()
{
	srand(time(0));

	GameEngine game;
	
	game.pushState(new GameStateStart(&game));
	game.gameLoop();

	return 0;
}