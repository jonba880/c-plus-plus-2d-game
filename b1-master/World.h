#ifndef WORLD_HPP
#define WORLD_HPP


#include "StaticObject.h"
#include "Graphic.h"
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <unordered_set>
#include <algorithm>


class World {
private:
	std::vector<Box> boxes;
	std::vector<sf::Vector2f> playerSpawnPoints;
	sf::Vector2u screen_size;
	std::unordered_set<int> forbidden_ground;
public:
	bool onForbiddenGround(sf::Vector2f c) {
		int key = hash(c.x,c.y);
		return (c.x < 0 || c.y < 0 || c.y > screen_size.y || c.x > screen_size.x || forbidden_ground.find(key) != forbidden_ground.end());
	}

	void setScreenSize(sf::Vector2u screen_size) {
		this->screen_size = screen_size;
	}
	
	void addBox(int x_box, int y_box , sf::Texture * texture) {
		Box box = Box(texture, true, x_box, y_box);
		boxes.push_back(box);
		sf::Vector2u size = box.getSprite().getTexture()->getSize();
		sf::Vector2f pos = box.getSprite().getPosition();
		getSquareArea(pos, forbidden_ground, size);			
	}
	


	World()	{

	

		playerSpawnPoints.push_back(sf::Vector2f(650, 500)); //Bottom-Middle Spawn
		playerSpawnPoints.push_back(sf::Vector2f(650, 200)); // Top-Middle Spawn
		playerSpawnPoints.push_back(sf::Vector2f(50, 50)); //Top-Left Spawn
		playerSpawnPoints.push_back(sf::Vector2f(1200, 650));
		sf::Texture * texture = new sf::Texture;
		texture->loadFromFile("textures/box.png");
		addBox(100,450, texture); //Top-Right Box
		addBox(900, 450, texture); // Bottom-Right Box
		addBox(500, 250, texture);
		addBox(900, 100, texture); //Bottom-Left Box
		addBox(100, 100, texture); //Top-Left Box
	}

	const std::vector<Box>& getBoxes() { return boxes;  }

	const sf::Vector2f& getRandomSpawn() 
	{ 
		return  playerSpawnPoints.at(rand() % playerSpawnPoints.size());
	}
	
	
};
#endif
