#include "GameStateWin.h"
#include "GameState.h"
#include <iostream>
#include <SFML/Graphics.hpp>

void GameStateWin::draw(const float dt)
{
		this->game->window.draw(_background);
}

void GameStateWin::update(const float dt)
{
	return;
}

void GameStateWin::handleInput()
{
	sf::Event event;

	while (this->game->window.pollEvent(event))
	{
		switch (event.type)
		{
		case sf::Event::Closed:
		{
			game->window.close();
			break;
		}
		case sf::Event::KeyPressed:
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
				game->window.close();
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return)) {
				this->jumpback();
				return;
			}
			break;
		}
		default:
			break;
		}
	}
}

GameStateWin::GameStateWin(GameEngine * game, int p1, int p2)
{
	this->game = game;
	
	if(p1 > p2)
	{
		setBackground("textures/winp1.png");
	}
	else
		setBackground("textures/winp2.png");
}

void GameStateWin::jumpback()
{
	this->game->popState();
}