#ifndef GRAPHIC_H
#define GRAPHIC_H
#include <SFML/Graphics.hpp>
#include <string>	
#include <memory>
#include <unordered_set>

class Graphic : public sf::Sprite
{
public:
	Graphic(sf::Texture * texture) ;
	void setOrgin(float w, float h) ;
	void rotate(float d);
	Graphic(std::string filename);
	Graphic() {}

	void changeTexture(std::string filename);

private:
	int _length;
	int _height;
	sf::Texture _texture;
};
void getSquareArea(sf::Vector2f pos, std::unordered_set<int> &body, sf::Vector2u size);

void getCircleArea(Graphic &g, std::unordered_set<int> &body, int diameter);
int hash(int x,int y);



#endif // !GRAPHIC_H





