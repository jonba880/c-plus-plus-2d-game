#ifndef GAMESTATECONTROLS_H
#define GAMESTATECONTROLS_H

#include <SFML/Graphics.hpp>
#include "Graphic.h"
#include "GameState.h"

class GameStateControls : public GameState
{
public:


	void draw(const float dt);
	void update(const float dt);
	void handleInput();

	GameStateControls(GameEngine* game);

private:
	void jumpback(){ this->game->popState(); }
};

#endif // !GAMESTATESTART_H
