#include <SFML/Graphics.hpp>

#include "GameState.h"
#include "GameStateControls.h"
#include "GameStatePause.h"


void GameStateControls::draw(const float dt)
{
	this->game->window.clear(sf::Color::Black);
	this->game->window.draw(_background);
}

void GameStateControls::update(const float dt)
{
	return;
}

void GameStateControls::handleInput()
{
	sf::Event event;

	while (this->game->window.pollEvent(event))
	{
		switch (event.type)
		{
			/* Close the window */
		case sf::Event::Closed:
		{
			game->window.close();
			break;
		}

		/* Input */
		case sf::Event::KeyPressed:
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
				game->window.close();
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::B))
			{
				jumpback();
				return;
			}	
			break;
		}
		default:
			break;
		}
	}
}

GameStateControls::GameStateControls(GameEngine * game)
{
	this->game = game;
	setBackground("textures/controlscreen.png");
}
