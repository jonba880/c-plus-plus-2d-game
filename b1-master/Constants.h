#ifndef CONSTANTS_H
#define CONSTANTS_H


/*
*		KONSTANTER
*/


#define SFML_VERSION 1
	
#if SFML_VERSION < 2
#define setFillColor setColor
#endif

const int
CHARACTER_SPEED = 4,
PROJECTILE_SPEED = 8,
ROTATION_DELAY = 4,
MOVING_DELAY = 0,
N_OF_PROJECTILES = 20,		
SHOOTING_DELAY = 15,
RELOAD_DELAY = 120;

const float
RESPAWN_DELAY = 2.0;

const double
CHARACTER_IMG_SCALE = 1,
PROJECTILE_IMG_SCALE = 0.3;


const double directions[16][2] = {
	{ 1, 0 },         //east			
	{ 0.89, 0.45 },
	{ 0.7, 0.7 },   //south east	
	{ 0.45, 0.89 },
	{ 0, 1 },		   //south				
	{ -0.45, 0.89 },
	{ -0.7, 0.7 },  //south west		
	{ -0.89, 0.45 },
	{ -1, 0 },        //west				
	{ -0.89, -0.45 },
	{ -0.7, -0.7 }, //north west	
	{ -0.45, -0.89 },
	{ 0, -1 },        //north				
	{ 0.45, -0.89 },
	{ 0.7, -0.7 },  //north east			
	{ 0.89, -0.45 }
};


#endif 


