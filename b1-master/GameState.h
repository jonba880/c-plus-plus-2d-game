#ifndef GAMESTATE_HPP
#define GAMESTATE_HPP

#include "GameEngine.h"
#include "Graphic.h"
#include <string>

class GameState
{

protected:
	
	Graphic _background;

public:

	GameEngine* game;

	virtual void draw(const float dt) = 0;
	virtual void update(const float dt) = 0;
	virtual void handleInput() = 0;
	void setBackground(std::string filename) { _background.changeTexture(filename); }


	
};

#endif // !GAME_STATE_HPP
