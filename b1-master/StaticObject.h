#ifndef STATICOBJECT_HPP
#define STATICOBJECT_HPP


#include "Graphic.h"
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>


class StaticObject {
protected:
	Graphic graphic;
	bool solid;
public:

	StaticObject(sf::Texture * image, bool issolid, int x,int y) : graphic{ image }, solid{ issolid } {
		graphic.setPosition(sf::Vector2f(x, y));
		
	}

	const Graphic& getSprite() {
		return graphic;
	}

	sf::Vector2f getPosition() {
		return graphic.getPosition();
	}

	Graphic& getGraphic() {
		return graphic;
	}

};

class Box: public StaticObject{
public:
	Box(sf::Texture* image, bool issolid, int x, int y) : StaticObject(image, true, x, y) {
	}
};




#endif
