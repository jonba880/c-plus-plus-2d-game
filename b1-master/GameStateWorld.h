#ifndef GAMESTATEWORLD_H
#define GAMETATEWORLD_H

#include <SFML/Graphics.hpp>
#include "Graphic.h"
#include "GameState.h"
#include "World.h"
#include "Information.h"
#include "Player.h"
#include <vector>
class GameStateWorld : public GameState
{
public:
	void draw(const float dt);
	void update(const float dt);
	void handleInput();
	bool winnerExist();
	Player &getOpponent(int i);
	void updateScore(int player_index, float dt);

	
	GameStateWorld(GameEngine* game);
private:
	Controls controls{ sf::Keyboard::Key::A, sf::Keyboard::Key::D,  sf::Keyboard::Key::W,  sf::Keyboard::Key::S,  sf::Keyboard::Key::Space };
	Controls controls2{ sf::Keyboard::Key::Left, sf::Keyboard::Key::Right,  sf::Keyboard::Key::Up,  sf::Keyboard::Key::Down,  sf::Keyboard::Key::Return };
	Player player { "textures/character2.png", "textures/projectile.png", controls };
	Player player2{ "textures/character2r.png", "textures/projectile.png", controls2 };
	std::vector<Player> players = { player,player2 };
	bool jump = false;

	Information info;
	void pausegame();
	void wingame();
	World world;

};

#endif // !GAMESTATEWORLD_H
