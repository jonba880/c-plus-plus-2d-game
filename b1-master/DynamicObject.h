#include <SFML/Graphics.hpp>
#include <string>
#include "Graphic.h"
#include <vector>
#include <iostream>
#include <memory>
#include "Constants.h"

#ifndef DYNAMICOBJECT_H
#define DYNAMICOBJECT_H




class DynamicObject {
protected:
	unsigned direction_key : 4;
	double speed_factor;
	Graphic graphic;
public:



	DynamicObject(std::string image, double w, double h, double speed) : graphic{ image}, direction_key{ 0 }, speed_factor{ speed } {
		graphic.scale(w,h);
	}

	DynamicObject(sf::Texture * image, double w, double h, double speed) : graphic{ image }, direction_key{ 0 }, speed_factor{ speed } {
		graphic.scale(w, h);
	}
	const Graphic& getSprite() {
		return graphic;
	}

	sf::Vector2f getPosition() {
		return graphic.getPosition();
	}

     void setPosition(sf::Vector2f pos) {
		 graphic.setPosition(pos);
	 }
	 void setPosition(float x, float y) {
		 graphic.setPosition(x, y);
	 }
	 void setDirection(int dir) {
		 direction_key = dir;
	 }

	 int getDirection() {
		 return direction_key;
	 }

	void rotateRight() {
		direction_key = direction_key + 1;
		graphic.rotate(22.5);
		
	}

	void rotateLeft() {
		direction_key = direction_key - 1;
		graphic.rotate(-22.5);
	}

	void setRotation() {
		graphic.setRotation(direction_key * 22.5);


	}

	sf::Vector2f getForwardPos() {
		sf::Vector2f next_pos;
		next_pos.x = graphic.getPosition().x + directions[direction_key][0] * speed_factor;
		next_pos.y = graphic.getPosition().y + directions[direction_key][1] * speed_factor;
		return next_pos;
	}
	
	Graphic& getGraphic () {
		return graphic;
	}
	sf::Vector2f getBackwardPos() {
		sf::Vector2f next_pos;
		next_pos.x = graphic.getPosition().x + directions[direction_key][0] * speed_factor * -1;
		next_pos.y = graphic.getPosition().y + directions[direction_key][1] * speed_factor * -1;
		return next_pos;
	}

	void moveForward() {
		float x_speed = directions[direction_key][0] * speed_factor;
		float y_speed = directions[direction_key][1] * speed_factor;
		graphic.move(x_speed, y_speed);
	
	}

	void moveForward(float x, float y) {
		float x_speed = directions[direction_key][0] * speed_factor;
		float y_speed = directions[direction_key][1] * speed_factor;
		graphic.move(x_speed * x, y_speed * y);
	
	}

	void moveBackwards() {
		float x_speed = directions[direction_key][0] * speed_factor ;
		float y_speed = directions[direction_key][1] * speed_factor ;
		graphic.move(x_speed*-1, y_speed*-1);
	}

};



class Character : public DynamicObject{
private:
	bool alive{ true };
public:													
  	Character(std::string image) : DynamicObject(image, CHARACTER_IMG_SCALE, CHARACTER_IMG_SCALE, CHARACTER_SPEED) {
		graphic.setOrgin(3, 2);
		graphic.setPosition(100, 300);
	}

	void live() { alive = true; }
	void die() { alive = false; }
	bool isAlive() { return alive; }


};

class Projectile : public DynamicObject {
private:
	bool alive;
public:
	Projectile(sf::Texture* image) : DynamicObject(image, PROJECTILE_IMG_SCALE, PROJECTILE_IMG_SCALE, PROJECTILE_SPEED), alive{ false } {
		graphic.setOrgin(2, 2);
	}
	
	void live() {alive = true;}
	void die() {alive = false;}
	bool isAlive() {return alive;}
};




#endif
