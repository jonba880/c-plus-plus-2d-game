#include "Graphic.h"

Graphic::Graphic(sf::Texture * texture) {
	setTexture((*texture));
}

void Graphic::setOrgin(float w, float h) {
	setOrigin(_texture.getSize().x / w, _texture.getSize().y / h);
}

void Graphic::rotate(float d) {
	setRotation(getRotation() + d);
}

void Graphic::changeTexture(std::string filename)
{
	_texture.loadFromFile(filename);
	setTexture(_texture);
}

Graphic::Graphic(std::string filename) {
	_texture.loadFromFile(filename);
	setTexture(_texture);
}

int hash(int x, int y) {
	return (x * 2000 + y);
}

void getSquareArea(sf::Vector2f pos, std::unordered_set<int> &body, sf::Vector2u size) {

	for (int x{ 0 }; x < size.x; x++) {
		for (int y{ 0 }; y < size.y; y++) {
			int x_pos = x + pos.x;
			int y_pos = y + pos.y;
			body.insert(hash(x_pos, y_pos));
		}
	}
}

void getCircleArea(Graphic &g, std::unordered_set<int> &body, int diameter) {
	int x_box = g.getPosition().x - diameter / 2;
	int y_box = g.getPosition().y - diameter / 2;
	for (int x{ 0 }; x < diameter; x++) {
		for (int y{ 0 }; y < diameter; y++) {
			int x_pos = x + x_box;
			int y_pos = y + y_box;
			body.insert(hash(x_pos, y_pos));
		}
	}
}




