#include "GameStatePause.h"
#include "GameState.h"
#include <iostream>
#include <SFML/Graphics.hpp>

void GameStatePause::draw(const float dt)
{
	//this->game->window.clear(sf::Color::Black);
	this->game->window.draw(_background);
}

void GameStatePause::update(const float dt)
{
	return;
}

void GameStatePause::handleInput()
{
	sf::Event event;

	while (this->game->window.pollEvent(event))
	{
		switch (event.type)
		{
		/* Close the window */
		case sf::Event::Closed:
		{
			game->window.close();
			break;
		}

		/* Input */
		case sf::Event::KeyPressed:
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
				game->window.close();
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Y))
				game->window.close();
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::N)) {
				this->jumpback();
				return;
			}
				
			break;
		}
		default:
			break;
		}
	}
}

GameStatePause::GameStatePause(GameEngine * game)
{
	this->game = game;
	setBackground("textures/pausemenu.png");
}

void GameStatePause::jumpback()
{	
	this->game->popState();	
}